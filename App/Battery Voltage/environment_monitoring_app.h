/*
 * environment_monitoring_app.h
 *
 *  Created on: 08-Feb-2022
 *      Author: jaroos
 */

#ifndef BATTERY_VOLTAGE_ENVIRONMENT_MONITORING_APP_H_
#define BATTERY_VOLTAGE_ENVIRONMENT_MONITORING_APP_H_

void BME680Config();
void BME680Read();


void airQualityInit();
void user_delay_ms(uint32_t period);
void BME680GPIO_Init();


int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);
int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);

#define BME680_FLOAT_POINT_COMPENSATION

typedef struct {
	uint32_t pressure; /* in mbar */

	uint16_t temperature; /* in �C   */

	uint16_t humidity; /* in %    */

	uint32_t gasResistance ; /* Air Quality Index*/

	uint16_t batteryLevel;

} energyHarvesting_t;

void energyHarvesting(energyHarvesting_t* sensor_data);

#endif /* BATTERY_VOLTAGE_ENVIRONMENT_MONITORING_APP_H_ */

